<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8">
<title>Welcome</title>
</head>
<body>
	<h1>${todo.title}</h1>

	<form:form modelAttribute="detailForm">
		<c:out value="${message}"></c:out>
		<div>
			<form:errors path="*" />
		</div>
		<form:textarea path="details" value="${todo.details}" />

		<input type="submit" value="詳細登録">
	</form:form>
</body>
</html>

