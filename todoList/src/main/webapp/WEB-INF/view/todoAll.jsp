<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="<c:url value="/resources/css/todo.css" />" rel="stylesheet">
<title>Welcome</title>
<script type="text/javascript">
	function onRadioButtonChange() {
		check1 = document.form1.Radio1.checked;
		check2 = document.form1.Radio2.checked;
		var obj1 = document.getElementById("finished");
		var obj2 = document.getElementById("unfinished");


		if (check1 == true) {
			obj1.classList.remove("finish");
			obj1.classList.add("todo");
		} else if (check2 == true) {
			obj2.classList.remove("unfinish");
			obj2.classList.add("todo");
		}
		if (check1 == false) {
			obj1.classList.remove("todo");
			obj1.classList.add("finish");
		} else if (check2 == false) {
			obj2.classList.remove("todo");
			obj2.classList.add("unfinish");
		}


	}
</script>
</head>
<body>
	<form name="form1" action="">
		<input id="Radio1" name="RadioGroup1" type="radio"
			onchange="onRadioButtonChange()" /> <label for="Radio1">完了</label><br />

		<input id="Radio2" name="RadioGroup1" type="radio"
			onchange="onRadioButtonChange()" /> <label for="Radio2">取り組み中</label><br />

	</form>

	<form:form modelAttribute="searchForm" action="/todoList/search"
		method="GET">
		<form:input path="searchWord" />
		<input type="submit" value="絞り込み">
	</form:form>

	<form:form modelAttribute="todoForm" action="/todoList/">
		<c:out value="${message}"></c:out>
		<div>
			<form:errors path="*" />
		</div>

		<form:input path="title" />
		<input type="submit" value="todoList追加">
	</form:form>


	<c:forEach items="${todos}" var="todo">

		<div id="finished" class='finish'>
			<c:if test="${ todo.reached == 0}">
				<c:out value="${todo.title}"></c:out>
				<form:form modelAttribute="reachedForm" action="/todoList/reached">
					<form:hidden path="id" value="${todo.id}" />
					<form:hidden path="reached" value="1" />
					<input type="submit" value="完了">
				</form:form>
				<a href="${pageContext.request.contextPath}/todo/${todo.id}/"><input
					type="submit" value="編集"></a>

				<form:form modelAttribute="deleteForm" action="/todoList/delete"
					onsubmit="deleteCheck()">
					<form:hidden path="id" value="${todo.id}" />
					<input type="submit" value="削除">
				</form:form>
			</c:if>
		</div>

		<div id="unfinished" class='unfinish'>
			<c:if test="${ todo.reached == 1}">
				<c:out value="${todo.title}"></c:out>
				<form:form modelAttribute="reachedForm" action="/todoList/reached">
					<form:hidden path="id" value="${todo.id}" />
					<form:hidden path="reached" value="0" />
					<input type="submit" value="未完了">
				</form:form>
				<a href="${pageContext.request.contextPath}/todo/${todo.id}/"><input
					type="submit" value="編集"></a>

				<form:form modelAttribute="deleteForm" action="/todoList/delete"
					onsubmit="deleteCheck()">
					<form:hidden path="id" value="${todo.id}" />
					<input type="submit" value="削除">
				</form:form>
			</c:if>
		</div>


		<script>
			function deleteCheck() {
				var result = window.confirm('この項目を削除します。よろしいですか？');

				if (result == false) {
					return false;
				}
			}
		</script>
	</c:forEach>
</body>
</html>