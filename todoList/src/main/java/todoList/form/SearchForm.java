package todoList.form;

import org.hibernate.validator.constraints.NotEmpty;

public class SearchForm {
	@NotEmpty
	private String searchWord;
	private int id;
	private String title;

	public String getSearchWord() {
		return searchWord;
	}
	public void setSearchWord(String searchWord) {
		this.searchWord = searchWord;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
}
