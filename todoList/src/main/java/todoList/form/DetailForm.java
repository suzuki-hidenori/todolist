package todoList.form;

import todoList.validator.annotation.Detail;

public class DetailForm {
	private int id;
	@Detail
	private String details;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}

}
