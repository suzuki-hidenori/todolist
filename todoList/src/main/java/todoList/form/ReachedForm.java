package todoList.form;

public class ReachedForm {
	private int reached;
	private int id;

	public int getReached() {
		return reached;
	}
	public void setReached(int reached) {
		this.reached = reached;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
