package todoList.dto.todo;

public class TodoDto {
	private int id;
	private String title;
	private String details;
	private int reached;
	private String searchWord;
	


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String[] getSplitedTitle() {
		return title.split("\n");
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public int getReached() {
		return reached;
	}

	public void setReached(int reached) {
		this.reached = reached;
	}

	public String getSearchWord() {
		return searchWord;
	}

	public void setSearchWord(String searchWord) {
		this.searchWord = searchWord;
	}
	


}