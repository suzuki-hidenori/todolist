package todoList;

import java.util.List;

import javax.validation.Valid;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import todoList.dto.todo.TodoDto;
import todoList.form.DeleteForm;
import todoList.form.DetailForm;
import todoList.form.ReachedForm;
import todoList.form.SearchForm;
import todoList.form.TodoForm;
import todoList.service.TodoService;

@Controller
public class TestController {

	@Autowired
	private TodoService TodoService;

	@RequestMapping(value = "/todo/{id}", method = RequestMethod.GET)
	public String index(Model model, @PathVariable Integer id) {
		TodoDto todo = TodoService.getTodo(id);
		DetailForm form = new DetailForm();

		form.setId(todo.getId());
		form.setDetails(todo.getDetails());

		model.addAttribute("detailForm", form);
		model.addAttribute("todo", todo);
		return "index";
	}

	@RequestMapping(value = "/todo/{id}/", method = RequestMethod.POST)
	public String todoUpdate(@Valid @ModelAttribute DetailForm form, BindingResult result, Model model,
			@PathVariable Integer id) {
		if (result.hasErrors()) {

			model.addAttribute("message", "詳細を記入してください");
			model.addAttribute("detail", "エラー");
			TodoDto todo = TodoService.getTodo(id);
			model.addAttribute("detailForm", form);
			model.addAttribute("todo", todo);
			return "index";

		} else {
			TodoDto dto = new TodoDto();
			BeanUtils.copyProperties(form, dto);

			int count = TodoService.updateTodo(dto);

			Logger.getLogger(TestController.class).log(Level.INFO, "更新件数は" + count + "件です。");
		}
		return "redirect:/";
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String todoAll(Model model) {

		List<TodoDto> todos = TodoService.getTodoAll();
		DeleteForm deleteForm = new DeleteForm();
		ReachedForm reachedForm = new ReachedForm();
		SearchForm searchForm = new SearchForm();
		TodoForm form = new TodoForm();

		reachedForm.setReached(reachedForm.getReached());
		reachedForm.setId(reachedForm.getId());

		model.addAttribute("deleteForm", deleteForm);
		model.addAttribute("reachedForm", reachedForm);
		model.addAttribute("todoForm", form);
		model.addAttribute("todos", todos);
		model.addAttribute("searchForm", searchForm);

		return "todoAll";
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public String todoInsert(@Valid @ModelAttribute TodoForm form, BindingResult result, Model model) {
		if (result.hasErrors()) {

			model.addAttribute("message", "タイトルを記入してください");
			model.addAttribute("title", "エラー");
			DeleteForm deleteForm = new DeleteForm();
			ReachedForm reachedForm = new ReachedForm();
			SearchForm searchForm = new SearchForm();
			TodoForm form1 = new TodoForm();

			model.addAttribute("deleteForm", deleteForm);
			model.addAttribute("reachedForm", reachedForm);
			model.addAttribute("todoForm", form1);
			model.addAttribute("searchForm", searchForm);
			return "todoAll";

		} else {
			int count = TodoService.insertTodo(form.getTitle());
			Logger.getLogger(TestController.class).log(Level.INFO, "挿入件数は" + count + "件です。");
		}
		return "redirect:/";
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public String todoSearch(@ModelAttribute TodoDto dto, Model model) {
		List<TodoDto> todos = TodoService.getTodoSearch(dto);
		DeleteForm deleteForm = new DeleteForm();
		ReachedForm reachedForm = new ReachedForm();
		SearchForm searchForm = new SearchForm();
		TodoForm form = new TodoForm();

		model.addAttribute("deleteForm", deleteForm);
		model.addAttribute("reachedForm", reachedForm);
		model.addAttribute("searchForm", searchForm);
		model.addAttribute("todoForm", form);
		model.addAttribute("todos", todos);
		return "todoAll";
	}

	@RequestMapping(value = "/reached", method = RequestMethod.POST)
	public String todoDetail(@ModelAttribute ReachedForm reachedForm, Model model) {
		TodoDto dto = new TodoDto();
		BeanUtils.copyProperties(reachedForm, dto);
		int count = TodoService.updateTodoList(dto);

		Logger.getLogger(TestController.class).log(Level.INFO, "完了ボタン" + count + "件です。");
		return "redirect:/";
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String todoDelete(@ModelAttribute DeleteForm deleteForm) {
		int count = TodoService.deleteTodo(deleteForm.getId());

		Logger.getLogger(TestController.class).log(Level.INFO, "削除件数は" + count + "件です。");
		return "redirect:/";
	}


}
