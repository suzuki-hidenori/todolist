<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Welcome</title>
</head>
<body>
	<form:form modelAttribute="searchForm" action="/todoList/search" method="POST" >
		<form:input path="searchWord" />
		<input type="submit" value="絞り込み">
	</form:form>

	<form:form modelAttribute="todoForm" action="/todoList/">
		<form:input path="title" />
		<input type="submit" value="todoList追加">
	</form:form>


	<c:forEach items="${todos}" var="todo">

		<p>
			<c:if test="${todo.reached == 0 }">
				<c:out value="${todo.title}"></c:out>


				<c:if test="${reachedForm.reached == 0 }">
					<form:form modelAttribute="reachedForm" action="/todoList/reached">
						<form:hidden path="id" value="${todo.id}" />
						<form:hidden path="reached" value="1" />
						<input type="submit" value="完了">
					</form:form>
				</c:if>
				<c:if test="${reachedForm.reached == 1 }">
					<form:form modelAttribute="reachedForm" action="/todoList/reached">
						<form:hidden path="id" value="${todo.id}" />
						<form:hidden path="reached" value="0" />
						<input type="submit" value="未完了">
					</form:form>
				</c:if>

				<a href="${pageContext.request.contextPath}/todo/${todo.id}/"><input
					type="submit" value="編集"></a>

				<form:form modelAttribute="deleteForm" action="/todoList/delete">
					<form:hidden path="id" value="${todo.id}" />
					<input type="submit" value="削除">
				</form:form>
			</c:if>
		</p>
	</c:forEach>
</body>
</html>